﻿using Libreria;

namespace SOA
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "AnimalSvc" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione AnimalSvc.svc o AnimalSvc.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class AnimalSvc : IAnimalSvc
    {
        public string ObtenerAlimento(int tipoAnimal)
        {

            return new Fachada().ObtenerAlimento(tipoAnimal);
        }
    }
}
