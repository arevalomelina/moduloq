﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SOA
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IAnimalSvc" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IAnimalSvc
    {
        [OperationContract]
        string ObtenerAlimento(int tipoAnimal);
    }
}
