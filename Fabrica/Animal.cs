﻿using Fabrica.interfaz;
using Fabrica.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrica
{
    public class Animal 
    {
        public string obtenerAlimento(TipoAnimal tipo)
        {
            IAnimal animal = new FabricaAnimales().obtenerAnimal(tipo);
            return animal.obtenerAlimento();
        }

        public string obtenerDesplazamiento(TipoAnimal tipo)
        {
            IAnimal animal = new FabricaAnimales().obtenerAnimal(tipo);
            return animal.obtenerDesplazamiento();
        }

        public string obtenerTipoAnimal(TipoAnimal tipo)
        {
            IAnimal animal = new FabricaAnimales().obtenerAnimal(tipo);
            return animal.obtenerTipoAnimal();
        }
    }
}
