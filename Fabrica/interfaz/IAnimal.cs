﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrica.interfaz
{
    public interface IAnimal
    {
        string obtenerTipoAnimal();

        string obtenerDesplazamiento();

        string obtenerAlimento();
    }
}
