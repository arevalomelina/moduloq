﻿using Fabrica.adaptador;
using Fabrica.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrica.interfaz
{
    public class FabricaAnimales
    {
        public IAnimal obtenerAnimal(TipoAnimal tipo)
        {
            IAnimal animal;
            switch (tipo)
            {
                case TipoAnimal.Terrestres: animal = new Terrestres();
                    break;
                case TipoAnimal.Acuatico: animal = new Acuatico();
                    break;
                default: animal = new aereos();
                    break;
            }
            
            return animal;
        }
    }
}
