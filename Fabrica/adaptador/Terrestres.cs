﻿using Fabrica.interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrica.adaptador
{
    public class Terrestres : IAnimal
    {
        public string obtenerAlimento()
        {
            return "Los animales comen de todo";
        }

        public string obtenerDesplazamiento()
        {
            return "caminan o se arrastran";
        }

        public string obtenerTipoAnimal()
        {
            return "perro o serpiente";
        }
    }
}
