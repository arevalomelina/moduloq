﻿using Fabrica.interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrica.adaptador
{
    public class aereos : IAnimal
    {
        public string obtenerAlimento()
        {
            return "frutas o semillas";
        }

        public string obtenerDesplazamiento()
        {
            return "volar";
        }

        public string obtenerTipoAnimal()
        {
            return "aves";
        }
    }
}
