﻿using Fabrica.interfaz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrica.adaptador
{
    public class Acuatico : IAnimal
    {
        public string obtenerAlimento()
        {
            return "Plancton";
        }

        public string obtenerDesplazamiento()
        {
            return "Nadan";
        }

        public string obtenerTipoAnimal()
        {
            return "Peces y moluscos";
        }
    }
}
