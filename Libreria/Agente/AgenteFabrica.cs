﻿using Fabrica;
using Fabrica.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libreria.Agente
{
    internal class AgenteFabrica
    {
        internal string ObtenerAnimal(int TipoAnimal)
        {
            Fabrica.util.TipoAnimal tipo = (Fabrica.util.TipoAnimal)TipoAnimal;
            return new Fabrica.Animal().obtenerTipoAnimal(tipo);
        }

        internal string ObtenerAlimento(int tipoAnimal)
        {
            TipoAnimal tipo = (TipoAnimal)tipoAnimal;
            return new Animal().obtenerAlimento(tipo);
        }
    }
}
