﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libreria
{
    public interface IFachada
    {
        string ObtenerNombrePersona(string nombre);
        string ObtenerNombrePersona(string nombre, string apellido);
    }
}
