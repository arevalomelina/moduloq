﻿using Libreria.Agente;
using Libreria.Logica;
using Libreria.utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libreria
{
   public class Fachada:IFachada
    {

        public string ObtenerNombrePersona()
        {
            //Persona objPersona = new Persona();
            //return objPersona.ObtenerNombre();
            return singleton.Nombre;
        }

        public string ObtenerNombrePersona(string nombre)
        {
            Persona objPersona = new Persona(nombre);
            return  objPersona.ObtenerNombre();
        }

        public string ObtenerNombrePersona(string nombre, string apellido)
        {
            throw new NotImplementedException();
        }

        public string Saludo(string nombre)
        {
            Persona objPersona = new Persona(nombre);
            return "Hola " + objPersona.ObtenerNombre();
        }

        public string ObtenerAnimal(int tipoAnimal)
        {
            return new AgenteFabrica().ObtenerAnimal(tipoAnimal);
        }

        public string ObtenerAlimento(int tipoAnimal)
        {
            return new AgenteFabrica().ObtenerAlimento(tipoAnimal);
        }
    }
}
