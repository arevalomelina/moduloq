﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libreria.Logica
{
    internal class Persona
    {
        //Definicion de propiedades
        private string Nombre { get; set; }

        //definicion del constructor

        internal Persona()
        {
            this.Nombre = "Melina";
        }

        internal Persona(string nombre)
        {
            this.Nombre = nombre;
        }

        /// <summary>
        /// Metodo para obtener el nombre de una persona
        /// </summary>
        /// <returns></returns>
        internal string ObtenerNombre()
        {
            return this.Nombre;
        }
    }
}
