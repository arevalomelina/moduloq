﻿using Libreria.utilidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Este es el namespace general
/// </summary>
namespace Consola
{
    /// <summary>
    /// Esta es la clase program que me va a ejecutar la consola
    /// </summary>
    class Program
    {
        /// <summary>
        /// Este es el metodo principal
        /// </summary>
        /// <param name="args">Contiene los argumentos necesarios para ejecutar el metodo</param>
        static void Main(string[] args)
        {
            //Este es un comentario inofensivo
            //TODO: Escribir el código
            /*
            #region Consola
            Console.Write("Hola");
            Console.WriteLine("Mi nombre es");
            Console.Write(ConfigurationManager.AppSettings["Nombre"].ToString());
            #endregion

            #region Clase 2
            int x = 5;

            if (x > (int)EnumEjemplo.a)
            {
                Console.WriteLine("Es mayor");
            }
            else if(x==0)
            {
                Console.WriteLine("Es cero");
            }
            else
            {
                Console.WriteLine("Es menor");
            }

            int b = (int)EnumEjemplo.b;
            Console.WriteLine(b);

            #endregion
            
            List<string> lstNombres = new List<string>();
            lstNombres.Add("Bryan");
            lstNombres.Add("Fernando");
            lstNombres.Add("Luis");

            foreach(string item in lstNombres)
            {
                continue;
            }

            Console.WriteLine(lstNombres[2]);


            Dictionary<string, int> dictPrecios;
            dictPrecios = new Dictionary<string, int>();

            dictPrecios.Add("papa", 1200);
            dictPrecios.Add("arroz", 3100);
            dictPrecios.Add("carne", 8000);

            int precio = dictPrecios["arroz"];
            
            Console.WriteLine(precio);

            dictPrecios["arroz"] = 5000;

            precio = dictPrecios["arroz"];

            Console.WriteLine(precio);

            */
            Libreria.Fachada fachada;
            fachada = new Libreria.Fachada();
            //string nombre = singleton.Nombre;//fachada.ObtenerNombrePersona();
            //string saludo = singleton.Adios(); //fachada.Saludo("Camilo");
            //string otro = fachada.ObtenerNombrePersona();
            //Console.WriteLine(nombre);
            Console.WriteLine("Escoja un tipo de animal: (1-Terrestre, 2- Aereo, 3-Acuatico)");
            int tipo = int.Parse(Console.ReadLine());
            string animal = fachada.ObtenerAnimal(tipo);
            Console.Read();
            /*Este es mi comentario final
             y está en dos lineas*/
        }

        /// <summary>
        /// Enumeracion de ejemplo
        /// </summary>
        public enum EnumEjemplo
        {
            /// <summary>
            /// Primer valor
            /// </summary>
            a = 10,
            /// <summary>
            /// Segundo valor
            /// </summary>
            b = 2
        }
    }
}
